package com.example.androidtestingisfun;

import android.content.Intent;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.text.Html;

import com.example.androidtestingisfun.chargeback.models.ChargebackContentResult;
import com.example.androidtestingisfun.chargeback.ui.ChargebackActivity;
import com.example.androidtestingisfun.core.App;
import com.example.androidtestingisfun.util.RestServiceMock;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ChargebackActivityLazyInitExampleTest {



    @Rule
    public ActivityTestRule<ChargebackActivity> mActivityRule = new ActivityTestRule<>(ChargebackActivity.class, true, false);

    ChargebackContentResult contentResult;

    // WARNING **********  THESE MAY FAIL IF YOU DON'T TURN OFF ANIMATIONS ON THE DEVICE

    @Before
    public void setup() {

        contentResult = RestServiceMock.getHappyChargebackResult();

        App.screenContents.put(ChargebackActivity.class, contentResult);
        Intent intent = new Intent();
        mActivityRule.launchActivity(intent);

    }

    @Test
    public void onCreated_contentFromPresenterIsDisplayed() {

        onView(withId(R.id.chargeback_title)).check(matches(withText(contentResult.title)));
        onView(withId(R.id.chargeback_reason1)).check(matches(withText(contentResult.reason_details.get(0).title)));
        onView(withId(R.id.chargeback_reason2)).check(matches(withText(contentResult.reason_details.get(1).title)));
        String str = Html.fromHtml(contentResult.comment_hint).toString();
        onView(withId(R.id.chargeback_reason_et)).check(matches(TestUtil.withTextHint(str)));
        //onView(withId(R.id.chargeback_primary_bt)).check(matches(withHint(presenter.primaryAction.title)));
        //onView(withId(R.id.chargeback_secondary_bt)).check(matches(withHint(presenter.secondaryAction.title)));
    }


    @Test
    public void setCardLocked_showsAndHidesCorrectViews(){

        mActivityRule.getActivity().setCardLocked(true);
        onView(withId(R.id.group_card_locked)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE.VISIBLE)));
        onView(withId(R.id.group_card_unlocked)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE.GONE)));

    }



}