package com.example.androidtestingisfun;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.text.Html;

import com.example.androidtestingisfun.chargeback.models.ChargebackContentResult;
import com.example.androidtestingisfun.chargeback.presenter.ChargebackPresenter;
import com.example.androidtestingisfun.chargeback.ui.ChargebackActivity;
import com.example.androidtestingisfun.util.RestServiceMock;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ChargebackActivityTest {


    @Rule
    public ActivityTestRule<ChargebackActivity> mActivityRule = new ActivityTestRule<>(ChargebackActivity.class);

    // *************** NOTE: Running this requires a custom test runner class and may require a customized
    // test run configuration on your IDE

    @Before
    public void setup() {

       //MockitoAnnotations.initMocks(this);
        System.setProperty("dexmaker.dexcache", InstrumentationRegistry.getTargetContext().getCacheDir().getPath());

    }

    @Test
    public void onCreated_contentFromPresenterIsDisplayed() {

        ChargebackPresenter presenter = mActivityRule.getActivity().presenter;

        onView(withId(R.id.chargeback_title)).check(matches(withText(presenter.titleLabel)));
        onView(withId(R.id.chargeback_reason1)).check(matches(withText(presenter.reason1.title)));
        onView(withId(R.id.chargeback_reason2)).check(matches(withText(presenter.reason2.title)));
        String str = Html.fromHtml(presenter.commentHint).toString();
        onView(withId(R.id.chargeback_reason_et)).check(matches(TestUtil.withTextHint(str)));
        onView(withId(R.id.chargeback_primary_bt)).check(matches(withText(presenter.primaryAction.title)));
        onView(withId(R.id.chargeback_secondary_bt)).check(matches(withText(presenter.secondaryAction.title)));
    }


    @Test
    public void setCardLocked_showsAndHidesCorrectViews(){

        mActivityRule.getActivity().setCardLocked(true);
        onView(withId(R.id.group_card_locked)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE.VISIBLE)));
        onView(withId(R.id.group_card_unlocked)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE.GONE)));

    }

    @Test
    public void clickPrimary_callsPresenterPrimaryAction() {
        // setup
        ChargebackActivity activity = mActivityRule.getActivity();
        ChargebackPresenter  presenter = activity.presenter;
        ChargebackPresenter spy = spy(presenter);
        activity.presenter = spy;
        doNothing().when(spy).onPrimaryUserAction();

        // exercise
        onView(withId(R.id.chargeback_primary_bt)).perform(click());
        // check
        verify(spy).onPrimaryUserAction();
    }

    @Test
    public void clickPrimary_callsPresenterSecondaryAction() {
        // setup
        ChargebackActivity activity = mActivityRule.getActivity();
        ChargebackPresenter  presenter = activity.presenter;
        ChargebackPresenter spy = spy(presenter);
        activity.presenter = spy;
        doNothing().when(spy).onSecondaryUserAction();

        // exercise
        onView(withId(R.id.chargeback_secondary_bt)).perform(click());
        // check
        verify(spy).onSecondaryUserAction();
    }





}