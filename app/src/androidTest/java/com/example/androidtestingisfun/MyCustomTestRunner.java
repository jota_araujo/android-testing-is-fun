package com.example.androidtestingisfun;

import android.app.Application;
import android.support.test.runner.AndroidJUnitRunner;

import com.example.androidtestingisfun.chargeback.ui.ChargebackActivity;
import com.example.androidtestingisfun.chargeback.ui.NoticeActivity;
import com.example.androidtestingisfun.core.App;
import com.example.androidtestingisfun.util.RestServiceMock;

public class MyCustomTestRunner extends AndroidJUnitRunner {
    @Override
    public void callApplicationOnCreate(Application application) {

        App.screenContents.put(NoticeActivity.class, RestServiceMock.getHappyNoticeResult());
        App.screenContents.put(ChargebackActivity.class, RestServiceMock.getHappyChargebackResult());

        super.callApplicationOnCreate(application);
    }


}