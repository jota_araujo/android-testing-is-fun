package com.example.androidtestingisfun;

import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.example.androidtestingisfun.chargeback.presenter.NoticePresenter;
import com.example.androidtestingisfun.chargeback.ui.NoticeActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class NoticeActivityTest {


    @Rule
    public ActivityTestRule<NoticeActivity> mActivityRule = new ActivityTestRule(NoticeActivity.class);

    // *************** NOTE: Running this requires a custom test runner class and may require a customized
    // test run configuration on your IDE

    @Before
    public void setup() {
        //MockitoAnnotations.initMocks(this);
        System.setProperty("dexmaker.dexcache", InstrumentationRegistry.getTargetContext().getCacheDir().getPath());
    }

    @Test
    public void onCreated_contentFromPresenterIsDisplayed() {

        NoticePresenter presenter = mActivityRule.getActivity().presenter;

        onView(withId(R.id.notice_title_label)).check(matches(withText(presenter.titleLabel)));
        onView(withId(R.id.notice_description_label)).check(matches(withText(presenter.descriptionLabel)));
        onView(withId(R.id.notice_primary_bt)).check(matches(withText(presenter.primaryButtonLabel)));
        onView(withId(R.id.notice_secondary_bt)).check(matches(withText(presenter.secondaryButtonLabel)));
    }

    @Test
    public void clickPrimary_callsPresenterPrimaryAction() {
        //setup
        NoticeActivity activity = mActivityRule.getActivity();
        NoticePresenter presenter = activity.presenter;
        NoticePresenter spy = spy(presenter);
        activity.presenter = spy;
        doNothing().when(spy).onPrimaryUserAction();
        // exercise
        onView(withId(R.id.notice_primary_bt)).perform(click());
        // check
        verify(spy).onPrimaryUserAction();
        // reset
        activity.presenter = presenter;
        Mockito.reset(spy);
    }

    @Test
    public void clickSecondary_callsPresenterSecondaryAction() {
        //setup
        NoticeActivity activity = mActivityRule.getActivity();
        NoticePresenter presenter = activity.presenter;
        NoticePresenter spy = spy(presenter);
        activity.presenter = spy;
        doNothing().when(spy).onSecondaryUserAction();
        // exercise
        onView(withId(R.id.notice_secondary_bt)).perform(click());
        // check
        verify(spy).onSecondaryUserAction();
        // reset
        activity.presenter = presenter;
        Mockito.reset(spy);

    }

}