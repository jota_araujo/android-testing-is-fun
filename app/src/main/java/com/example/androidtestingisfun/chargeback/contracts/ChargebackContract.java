package com.example.androidtestingisfun.chargeback.contracts;

/**
 * Created by joao on 28/02/16.
 */
public interface ChargebackContract {

    interface View {

        void applyScreenContent();

        void setCardLocked(Boolean locked);

        void showLoader();

        void hideLoader();

        void showRequestSentSuccessful();

    }

    interface ViewMediator {

        void setReason1Selected(Boolean selected);

        void setReason2Selected(Boolean selected);

        void onPrimaryUserAction();

        void onSecondaryUserAction();

        void onClickDialogOK();

    }
}
