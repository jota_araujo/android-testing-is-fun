package com.example.androidtestingisfun.chargeback.contracts;

/**
 * Created by joao on 28/02/16.
 */
public interface MainContract {

    interface View {

        void showLoader();

        void hideLoader();

    }

    interface ViewMediator {

        void loadNextScreenContent();

    }
}
