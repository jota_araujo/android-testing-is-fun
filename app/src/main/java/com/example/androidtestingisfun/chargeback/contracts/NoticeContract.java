package com.example.androidtestingisfun.chargeback.contracts;

/**
 * Created by joao on 28/02/16.
 */
public interface NoticeContract {

    interface View {

        void applyScreenContent();

        void showLoader();

        void hideLoader();
    }

    interface ViewMediator {

        void onPrimaryUserAction();

        void onSecondaryUserAction();

    }
}
