
package com.example.androidtestingisfun.chargeback.models;

public class Action {

    public String title;
    public String action;

    public Action(){
        // required empty constructor
    }

    public Action(String title, String action){
        this.title = title;
        this.action = action;
    }

}
