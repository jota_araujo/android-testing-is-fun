
package com.example.androidtestingisfun.chargeback.models;

import java.util.List;

public class ChargebackContentResult {

    public String comment_hint;
    public String id;
    public String title;
    public Boolean autoblock;
    public List<ReasonDetail> reason_details;
    public ChargebackLinks links;

    public ChargebackContentResult(){
        // required empty constructor
    }

}
