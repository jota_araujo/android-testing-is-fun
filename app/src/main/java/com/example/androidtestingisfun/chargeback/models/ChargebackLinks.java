package com.example.androidtestingisfun.chargeback.models;

/**
 * Created by joao on 28/02/16.
 */
public class ChargebackLinks {

    public Link block_card;
    public Link unblock_card;
    public Link self;

    public ChargebackLinks(){
        // required empty constructor
    }

}
