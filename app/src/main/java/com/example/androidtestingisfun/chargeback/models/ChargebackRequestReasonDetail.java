package com.example.androidtestingisfun.chargeback.models;

/**
 * Created by joao on 03/03/16.
 */
public class ChargebackRequestReasonDetail {

    public String id;
    public Boolean response;


    public ChargebackRequestReasonDetail(String id, Boolean response ){
        this.id = id;
        this.response = response;
    }

}
