
package com.example.androidtestingisfun.chargeback.models;

public class Link {

    public String href;

    public Link(){
        // required empty constructor
    }

    public Link(String hrefStr) {
        this.href = hrefStr;
    }
}
