
package com.example.androidtestingisfun.chargeback.models;

public class ReasonDetail {

    public String id;
    public String title;

    public ReasonDetail(){
        // required empty constructor
    }

    public ReasonDetail(String id, String title){
        this.id = id;
        this.title = title;
    }

}
