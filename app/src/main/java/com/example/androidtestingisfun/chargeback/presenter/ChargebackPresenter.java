package com.example.androidtestingisfun.chargeback.presenter;

import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.androidtestingisfun.R;
import com.example.androidtestingisfun.chargeback.contracts.ChargebackContract;
import com.example.androidtestingisfun.chargeback.models.Action;
import com.example.androidtestingisfun.chargeback.models.ChargebackContentResult;
import com.example.androidtestingisfun.chargeback.models.ChargebackLinks;
import com.example.androidtestingisfun.chargeback.models.ChargebackRequestReasonDetail;
import com.example.androidtestingisfun.chargeback.models.ChargebackRequestResult;
import com.example.androidtestingisfun.chargeback.models.ChargebackResquest;
import com.example.androidtestingisfun.chargeback.models.ReasonDetail;
import com.example.androidtestingisfun.chargeback.rest.RestService;
import com.example.androidtestingisfun.chargeback.ui.ChargebackActivity;
import com.example.androidtestingisfun.core.App;
import com.example.androidtestingisfun.util.Util;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by joao on 28/02/16.
 */
public class ChargebackPresenter implements ChargebackContract.ViewMediator {

    public ChargebackActivity view;
    public RestService service;

    public String titleLabel;
    public ReasonDetail reason1;
    public ReasonDetail reason2;
    public String commentHint;
    public Action primaryAction;
    public Action secondaryAction;
    public Boolean userSelectedReason1 = false;
    public Boolean userSelectedReason2 = false;
    public ChargebackLinks links;

    public Scheduler schedulerForSubscribe;
    public Scheduler schedulerForObserve;


    public ChargebackPresenter(ChargebackActivity view, ChargebackContentResult screenContent, RestService service){

        this.view = view;
        this.service = service;
        parseScreenContent(screenContent);

        // FIXME: Doesn't match the requirements but I'll leave it as such for now
        view.setCardLocked(screenContent.autoblock);

    }

    public void parseScreenContent(ChargebackContentResult content){
        titleLabel = content.title;
        reason1 = content.reason_details.get(0);
        reason2 = content.reason_details.get(1);
        commentHint = content.comment_hint;
        // FIXME: I expected to receive those for consistency but I don't, so I'm adding dummies
        primaryAction = new Action("Continuar", "continue");
        secondaryAction = new Action("Cancelar", "cancel");
        links = content.links;
    }

    public void setReason1Selected(Boolean selected) {
        userSelectedReason1 = selected;
    }

    public void setReason2Selected(Boolean selected) {
        userSelectedReason2 = selected;
    }

    public void onPrimaryUserAction() {
        submitChargeBack();
    }

    @Override
    public void onSecondaryUserAction() {
        // TODO: come up with a better way to go back to the previous screen
        view.finish();
    }

    @Override
    public void onClickDialogOK() { // FIXME: this is really called anywhere
        view.findViewById(R.id.dialog_chargeback_sent).setVisibility(View.GONE);
    }

    public void requestLockCard(){ // FIXME: this is not really called anywhere
        service.requestLockCard(Util.getPath(links.block_card.href));
    }

    public void requestUnlockCard(){ // FIXME: this is not really called anywhere
        service.requestUnlockCard(Util.getPath(links.unblock_card.href));
    }

    public ChargebackResquest getChargebackRequestPayload(){
        String userComment = ((EditText) view.findViewById(R.id.chargeback_reason_et)).getText().toString();

        ChargebackResquest payload = new ChargebackResquest();
        payload.comment = userComment;
        payload.reason_details = new ChargebackRequestReasonDetail[2];
        payload.reason_details[0] = new ChargebackRequestReasonDetail(reason1.id, userSelectedReason1 );
        payload.reason_details[1] = new ChargebackRequestReasonDetail(reason1.id, userSelectedReason2 );

        return  payload;
    }

    public void submitChargeBack(){

        // POST to chargeback endpoint
        if(schedulerForSubscribe == null) schedulerForSubscribe = Schedulers.io();
        if(schedulerForObserve == null) schedulerForObserve = AndroidSchedulers.mainThread();

        Observable<ChargebackRequestResult> call = service.requestChargeback(Util.getPath(links.self.href), getChargebackRequestPayload());
        Subscription subscription = call
                .subscribeOn(schedulerForSubscribe)
                .observeOn(schedulerForObserve).subscribe(new Subscriber<ChargebackRequestResult>() {
                    @Override
                    public void onCompleted() {
                        Log.e("test", "request on completed");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("test", "request on error");
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException)e;
                            int code = response.code();
                            Util.showErrorUI(view, view.getString(R.string.rest_error_lame_excuse));
                        }
                    }

                    @Override
                    public void onNext(ChargebackRequestResult content) {
                        App app = (App) view.getApplication();
                        Log.e("test", "request on next");
                        if(content.status != null && content.status.equals(ChargebackRequestResult.STATUS_OK)){
                            view.showRequestSentSuccessful();
                        }else{
                            // TODO: show error alert;
                        }
                        //app.navigateToActivity(view, content,  ChargebackActivity.class);
                    }
                });

    }
}
