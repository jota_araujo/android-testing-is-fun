package com.example.androidtestingisfun.chargeback.presenter;

import android.util.Log;

import com.example.androidtestingisfun.R;
import com.example.androidtestingisfun.chargeback.models.NoticeContentResult;
import com.example.androidtestingisfun.chargeback.rest.RestService;
import com.example.androidtestingisfun.chargeback.ui.MainActivity;
import com.example.androidtestingisfun.chargeback.ui.NoticeActivity;
import com.example.androidtestingisfun.core.App;
import com.example.androidtestingisfun.util.Util;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by joao on 28/02/16.
 */
public class MainPresenter {


    public RestService service;
    public MainActivity view;

    public Scheduler schedulerForSubscribe;
    public Scheduler schedulerForObserve;

    public String noticeEndpointPath = "notice"; // TODO: load this information dynamically from the webservice root

    public MainPresenter(MainActivity view, RestService service){

        this.view = view;
        this.service = service;
        loadNextScreenContent();
    }

    public void loadNextScreenContent(){

        if(schedulerForSubscribe == null) schedulerForSubscribe = Schedulers.io();
        if(schedulerForObserve == null) schedulerForObserve = AndroidSchedulers.mainThread();

        Observable<NoticeContentResult> call = service.getNoticeScreenContent(noticeEndpointPath);
        Subscription subscription = call
                .subscribeOn(schedulerForSubscribe)
                .observeOn(schedulerForObserve).subscribe(new Subscriber<NoticeContentResult>() {
                    @Override
                    public void onCompleted() {
                        Log.e("test", "oncompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("test", "onerror");
                        // cast to retrofit.HttpException to get the response code
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException)e;
                            int code = response.code();
                            System.out.print(response.message());
                            e.printStackTrace();
                            Util.showErrorUI(view, view.getString(R.string.rest_error_lame_excuse));
                        }
                    }

                    @Override
                    public void onNext(NoticeContentResult content) {
                        App app = (App) view.getApplication();
                        app.navigateToActivity(view, content, NoticeActivity.class);
                    }
                });
    }


}
