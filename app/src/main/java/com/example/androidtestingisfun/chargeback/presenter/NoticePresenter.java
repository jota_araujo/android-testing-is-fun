package com.example.androidtestingisfun.chargeback.presenter;

import android.util.Log;

import com.example.androidtestingisfun.chargeback.models.Link;
import com.example.androidtestingisfun.chargeback.models.NoticeContentResult;
import com.example.androidtestingisfun.chargeback.models.ChargebackContentResult;
import com.example.androidtestingisfun.chargeback.contracts.NoticeContract;
import com.example.androidtestingisfun.chargeback.rest.RestService;
import com.example.androidtestingisfun.chargeback.ui.ChargebackActivity;
import com.example.androidtestingisfun.chargeback.ui.NoticeActivity;
import com.example.androidtestingisfun.core.App;
import com.example.androidtestingisfun.util.Util;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by joao on 28/02/16
 */
public class NoticePresenter implements NoticeContract.ViewMediator {

    public RestService service;
    public NoticeActivity view;

    public String titleLabel;
    public String descriptionLabel;
    public String primaryButtonLabel;
    public String secondaryButtonLabel;
    public Link cancelLink;
    public Link chargebackLink;

    public Scheduler schedulerForSubscribe;
    public Scheduler schedulerForObserve;


    public  NoticePresenter(NoticeActivity view, NoticeContentResult screenContent, RestService service){

        this.view = view;
        this.service = service;
        parseScreenContent(screenContent);
    }

    public void parseScreenContent(NoticeContentResult content){
        titleLabel = content.title;
        descriptionLabel = content.description;
        primaryButtonLabel = content.primary_action.title;
        secondaryButtonLabel = content.secondary_action.title;
        cancelLink = new Link("dummy"); // TODO: implement
        chargebackLink = content.links.chargeback;
    }

    @Override
    public void onPrimaryUserAction() {
        loadChargebackScreenContent();
    }

    @Override
    public void onSecondaryUserAction() {
        // TODO: come up with a better way to go back to the previous screen
        view.finish();
    }

    public void loadChargebackScreenContent(){

        if(schedulerForSubscribe == null) schedulerForSubscribe = Schedulers.io();
        if(schedulerForObserve == null) schedulerForObserve = AndroidSchedulers.mainThread();

        Observable<ChargebackContentResult> call = service.getChargebackScreenContent(Util.getPath(chargebackLink.href));
        Subscription subscription = call
                .subscribeOn(schedulerForSubscribe)
                .observeOn(schedulerForObserve).subscribe(new Subscriber<ChargebackContentResult>() {
                    @Override
                    public void onCompleted() {
                        Log.e("test", "oncompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("test", "onerror");
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException)e;
                            int code = response.code();
                            //Util.showErrorUI(view, view.getString(R.string.rest_error_lame_excuse));
                        }
                    }

                    @Override
                    public void onNext(ChargebackContentResult content) {
                        App app = (App) view.getApplication();
                        app.navigateToActivity(view, content,  ChargebackActivity.class);
                    }
                });
    }
}