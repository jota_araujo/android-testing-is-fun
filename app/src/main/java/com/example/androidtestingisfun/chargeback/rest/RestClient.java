package com.example.androidtestingisfun.chargeback.rest;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

/**
 * Created by joao on 28/02/16.
 */


public class RestClient {

    public static final String BASE_URL = "http://nu-mobile-hiring.herokuapp.com";

    private static Retrofit instance;

    private static MockRetrofit mockInstance;

    public static Retrofit getInstance(){

        if(instance != null) return instance;

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        instance = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        return instance;

    }

    public static MockRetrofit getMockInstance(){

        if( mockInstance != null) return mockInstance;

        // Create a MockRetrofit object with a NetworkBehavior which manages the fake behavior of calls.
        NetworkBehavior behavior = NetworkBehavior.create();
        mockInstance = new MockRetrofit.Builder(getInstance())
                .networkBehavior(behavior)
                .build();
        return mockInstance;

    }


}
