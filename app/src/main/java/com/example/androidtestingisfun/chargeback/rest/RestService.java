package com.example.androidtestingisfun.chargeback.rest;

import com.example.androidtestingisfun.chargeback.models.ChargebackRequestResult;
import com.example.androidtestingisfun.chargeback.models.ChargebackResquest;
import com.example.androidtestingisfun.chargeback.models.NoticeContentResult;
import com.example.androidtestingisfun.chargeback.models.ChargebackContentResult;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

public interface RestService {

    @GET("/{path}")
    Observable<NoticeContentResult> getNoticeScreenContent(@Path("path") String path);

    @GET("/{path}")
    Observable<ChargebackContentResult> getChargebackScreenContent(@Path("path") String path);

    @POST("/{path}")
    Observable<ChargebackRequestResult> requestChargeback(@Path("path") String path, @Body ChargebackResquest payload);

    @POST("/{path}")
    Observable<Object> requestLockCard(@Path("path") String path);

    @POST("/{path}")
    Observable<Object> requestUnlockCard(@Path("path") String path);

}