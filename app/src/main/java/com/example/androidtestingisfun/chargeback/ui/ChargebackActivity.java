package com.example.androidtestingisfun.chargeback.ui;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.androidtestingisfun.R;
import com.example.androidtestingisfun.core.App;
import com.example.androidtestingisfun.core.BaseActivity;
import com.example.androidtestingisfun.chargeback.contracts.ChargebackContract;
import com.example.androidtestingisfun.chargeback.presenter.ChargebackPresenter;
import com.example.androidtestingisfun.util.Util;

import butterknife.Bind;
import butterknife.OnClick;

public class ChargebackActivity extends BaseActivity implements ChargebackContract.View {


    @Bind(R.id.chargeback_title)
    TextView LabelTitle;

    @Bind(R.id.group_card_locked)
    LinearLayout GroupCardLocked;

    @Bind(R.id.group_card_unlocked)
    LinearLayout GroupCardUnlocked;

    @Bind(R.id.chargeback_reason1)
    TextView LabelReason1;

    @Bind(R.id.chargeback_reason2)
    TextView LabelReason2;

    @Bind(R.id.chargeback_reason_et)
    TextView FieldReasonDetail;

    @Bind(R.id.chargeback_primary_bt)
    Button ButtonPrimary;

    @Bind(R.id.chargeback_secondary_bt)
    Button ButtonSecondary;

    @Bind(R.id.dialog_chargeback_sent)
    LinearLayout DialogCharbackSent;

    public ChargebackPresenter presenter;

    // ************************** ACTIVITY INITIALIZATION BLOCK ******************************* //
    // **************************************************************************************** //

    @Override
    protected int getLayoutToInflate() {
        return R.layout.activity_chargeback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // Let's be DRY and delegate the initialization logic to the super class
    }

    @Override
    public void applyScreenContent() {
        LabelTitle.setText(presenter.titleLabel);
        LabelReason1.setText(presenter.reason1.title);
        LabelReason2.setText(presenter.reason2.title);
        if(Util.isValidNonEmptyString(presenter.commentHint) ){
            FieldReasonDetail.setHint(Html.fromHtml(presenter.commentHint));
        }
        ButtonPrimary.setText(presenter.primaryAction.title);
        ButtonSecondary.setText(presenter.secondaryAction.title);

    }
    @Override
    public void requestDependencies(App app) {
        presenter = app.providePresenter(this); // TODO: replace this silly hack with proper DI such as Dagger
    }

    // ************************** CONTRACT METHODS BLOCK ************************************** //
    // **************************************************************************************** //

    public void setCardLocked(Boolean locked){
        GroupCardLocked.setVisibility(locked ? View.VISIBLE : View.GONE);
        GroupCardUnlocked.setVisibility(locked ? View.GONE : View.VISIBLE);
    }

    public void showRequestSentSuccessful(){
        DialogCharbackSent.setVisibility(View.VISIBLE);
    }

    // ************************** USER INPUT METHODS BLOCK ************************************ //
    // **************************************************************************************** //


    @OnClick(R.id.chargeback_primary_bt)
    public void onClickPrimary(View v){
        presenter.onPrimaryUserAction();
    }

    @OnClick(R.id.chargeback_secondary_bt)
    public void onClickSecondary(View v){
        presenter.onSecondaryUserAction();
    }

    @OnClick(R.id.chargeback_toggle1)
    public void onToggleReason1(ToggleButton v){
        presenter.setReason1Selected(v.isSelected());
    }

    @OnClick(R.id.chargeback_toggle2)
    public void onToggleReason2(ToggleButton v){
        presenter.setReason2Selected(v.isSelected());
    }

    @OnClick(R.id.dialog_button)
    public void onDialogButton(View v){
        presenter.onClickDialogOK();
    }

}
