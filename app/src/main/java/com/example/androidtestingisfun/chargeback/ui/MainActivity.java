package com.example.androidtestingisfun.chargeback.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.androidtestingisfun.R;
import com.example.androidtestingisfun.chargeback.contracts.MainContract;
import com.example.androidtestingisfun.chargeback.presenter.MainPresenter;
import com.example.androidtestingisfun.core.App;
import com.example.androidtestingisfun.core.BaseActivity;

public class MainActivity extends BaseActivity implements MainContract.View {


    public MainPresenter presenter;

    // ************************** ACTIVITY INITIALIZATION BLOCK ******************************* //
    // **************************************************************************************** //

    @Override
    protected int getLayoutToInflate() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // Let's be DRY and delegate the initialization logic to the super class
    }

    @Override
    public void applyScreenContent() {
        // nothing to see here
    }

    @Override
    public void requestDependencies(App app) {
        presenter = app.providePresenter(this);
    }

    // ************************** CONTRACT METHODS BLOCK ************************************** //
    // **************************************************************************************** //


    // ************************** USER INPUT METHODS BLOCK ************************************ //
    // **************************************************************************************** //
}
