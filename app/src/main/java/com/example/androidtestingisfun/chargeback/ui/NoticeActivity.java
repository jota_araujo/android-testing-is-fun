package com.example.androidtestingisfun.chargeback.ui;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.androidtestingisfun.R;
import com.example.androidtestingisfun.core.App;
import com.example.androidtestingisfun.core.BaseActivity;
import com.example.androidtestingisfun.chargeback.contracts.NoticeContract;
import com.example.androidtestingisfun.chargeback.presenter.NoticePresenter;
import com.example.androidtestingisfun.util.Util;

import butterknife.Bind;
import butterknife.OnClick;

public class NoticeActivity extends BaseActivity implements NoticeContract.View {

    @Bind(R.id.notice_title_label)
    TextView LabelTitle;

    @Bind(R.id.notice_description_label)
    TextView LabelDescription;

    @Bind(R.id.notice_primary_bt)
    Button ButtonPrimary;

    @Bind(R.id.notice_secondary_bt)
    Button ButtonSecondary;

    public NoticePresenter presenter;

    // ************************** ACTIVITY INITIALIZATION BLOCK ******************************* //
    // **************************************************************************************** //

    @Override
    protected int getLayoutToInflate() {
        return R.layout.activity_notice;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // Let's be DRY and delegate the initialization logic to the super class
    }

    @Override
    public void applyScreenContent() {
        LabelTitle.setText(presenter.titleLabel);
        if(Util.isValidNonEmptyString(presenter.descriptionLabel) ){
            LabelDescription.setText( Html.fromHtml(presenter.descriptionLabel));
        }
        ButtonPrimary.setText(presenter.primaryButtonLabel);
        ButtonSecondary.setText(presenter.secondaryButtonLabel);
    }

    @Override
    public void requestDependencies(App app) {
        presenter = app.providePresenter(this); // TODO: replace this silly hack with proper DI such as Dagger
    }

    // ************************** CONTRACT METHODS BLOCK ************************************** //
    // **************************************************************************************** //

    // ************************** USER INPUT METHODS BLOCK ************************************ //
    // **************************************************************************************** //

    @OnClick(R.id.notice_primary_bt)
    public void onClickPrimary(View v){
        presenter.onPrimaryUserAction();
    }

    @OnClick(R.id.notice_secondary_bt)
    public void onClickSecondary(View v){
        presenter.onSecondaryUserAction();
    }





}
