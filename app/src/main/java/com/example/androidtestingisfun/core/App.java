package com.example.androidtestingisfun.core;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.example.androidtestingisfun.chargeback.models.ChargebackContentResult;
import com.example.androidtestingisfun.chargeback.models.NoticeContentResult;
import com.example.androidtestingisfun.chargeback.presenter.ChargebackPresenter;
import com.example.androidtestingisfun.chargeback.presenter.MainPresenter;
import com.example.androidtestingisfun.chargeback.presenter.NoticePresenter;
import com.example.androidtestingisfun.chargeback.rest.RestClient;
import com.example.androidtestingisfun.chargeback.rest.RestService;
import com.example.androidtestingisfun.chargeback.ui.ChargebackActivity;
import com.example.androidtestingisfun.chargeback.ui.MainActivity;
import com.example.androidtestingisfun.chargeback.ui.NoticeActivity;
import com.example.androidtestingisfun.util.RestServiceMock;

import java.util.HashMap;

import retrofit2.Retrofit;

public class App extends Application
{
    @Override
    public void onCreate()
    {
        super.onCreate();
        //initSingletons();
    }

    public static HashMap<Class, Object> screenContents = new HashMap<>();

    public void navigateToActivity(Context context, Object content, Class activityClass) {
        screenContents.put(activityClass, content);
        Intent intent = new Intent( getApplicationContext(), activityClass);
        context.startActivity(intent);
    }

    public MainPresenter providePresenter(MainActivity view){
        Retrofit retrofit = RestClient.getInstance();
        return new MainPresenter(view, retrofit.create(RestService.class));
    }

    public NoticePresenter providePresenter(NoticeActivity view){
        Retrofit retrofit = RestClient.getInstance();
        return new NoticePresenter(view,
                (NoticeContentResult) screenContents.get(view.getClass()),
                retrofit.create(RestService.class));
    }

    public ChargebackPresenter providePresenter(ChargebackActivity view){
        Retrofit retrofit = RestClient.getInstance();
        ChargebackContentResult content = (ChargebackContentResult) screenContents.get(view.getClass());
        return new ChargebackPresenter(view,
                content,
                retrofit.create(RestService.class));
    }


}