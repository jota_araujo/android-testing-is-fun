// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.example.androidtestingisfun.core;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;


public abstract class BaseActivity extends AppCompatActivity
{

    public BaseActivity()
    {
    }

    protected abstract int getLayoutToInflate();

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(getLayoutToInflate());
        ButterKnife.bind(this);
        App app = (App) getApplication();
        requestDependencies(app);
        applyScreenContent();

    }

    public abstract  void applyScreenContent();
    public abstract  void requestDependencies(App app);

    public void onStop()
    {
        super.onStop();
    }

    public void showLoader() {

    }

    public void hideLoader() {

    }

}
