package com.example.androidtestingisfun.util;

import com.example.androidtestingisfun.chargeback.models.Action;
import com.example.androidtestingisfun.chargeback.models.ChargebackContentResult;
import com.example.androidtestingisfun.chargeback.models.ChargebackLinks;
import com.example.androidtestingisfun.chargeback.models.ChargebackRequestResult;
import com.example.androidtestingisfun.chargeback.models.ChargebackResquest;
import com.example.androidtestingisfun.chargeback.models.Link;
import com.example.androidtestingisfun.chargeback.models.NoticeContentResult;
import com.example.androidtestingisfun.chargeback.models.NoticeLinks;
import com.example.androidtestingisfun.chargeback.models.ReasonDetail;
import com.example.androidtestingisfun.chargeback.rest.RestService;

import java.util.ArrayList;

import retrofit2.http.Body;
import retrofit2.http.Path;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.Calls;
import rx.Observable;

public class RestServiceMock implements RestService {
    private final BehaviorDelegate<RestService> delegate;

    public RestServiceMock(BehaviorDelegate<RestService> delegate) {
        this.delegate = delegate;
    }

    public Observable<NoticeContentResult> getNoticeScreenContent(String path){
        return delegate.returning(Calls.response("test")).getNoticeScreenContent("notice");
    }


    public Observable<ChargebackContentResult> getChargebackScreenContent(String path) {
        return delegate.returning(Calls.response(getHappyChargebackResult())).getChargebackScreenContent("chargeback");
    }

    @Override
    public Observable<ChargebackRequestResult> requestChargeback(@Path("path") String path, @Body ChargebackResquest payload) {
        return null;
    }

    @Override
    public Observable<Object> requestLockCard(@Path("path") String path) {
        return null;
    }

    @Override
    public Observable<Object> requestUnlockCard(@Path("path") String path) {
        return null;
    }


    public static NoticeContentResult getHappyNoticeResult(){

        NoticeContentResult result = new NoticeContentResult();
        result.title = "some_title_label";
        result.description = "description";
        result.primary_action = new Action("notice_action_title_1", "notice_action_1");
        result.secondary_action = new Action("notice_action_title_2", "notice_action_2");
        result.links = new NoticeLinks();
        result.links.chargeback = new Link("chargeback");

        return result;
    }

    public static ChargebackContentResult getHappyChargebackResult(){

        ChargebackContentResult result = new ChargebackContentResult();
        result.title = "titleLabel";
        result.comment_hint = "<html>hello</html>";
        result.autoblock = true;
        result.reason_details = new ArrayList<>();
        result.reason_details.add( new ReasonDetail("reason_id_1", "reason_title_1"));
        result.reason_details.add( new ReasonDetail("reason_id_2", "reason_title_2"));
        result.links = new ChargebackLinks();


        return result;
    }
}