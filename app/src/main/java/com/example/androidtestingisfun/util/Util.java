package com.example.androidtestingisfun.util;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.util.EventLogTags;
import android.view.View;
import android.widget.EditText;

/**
 * Created by joao on 29/02/16.
 */
public class Util {

    public static Boolean isValidNonEmptyString(String str){
        return str != null && !str.isEmpty();
    }

    public static String getPath(String str){
        String str2 = str.replace("//", "");
        if(str2.contains("/")){
            int index = str2.indexOf("/");
            return str2.substring(index+1);
        }

        return str;
    }


    public static void showErrorUI(Context context, String message){

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Erro");
            builder.setMessage(message);
            builder.setPositiveButton("OK", null);
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



}
