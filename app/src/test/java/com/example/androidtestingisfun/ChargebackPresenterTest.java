package com.example.androidtestingisfun;

import com.example.androidtestingisfun.chargeback.models.ChargebackContentResult;
import com.example.androidtestingisfun.chargeback.presenter.ChargebackPresenter;
import com.example.androidtestingisfun.chargeback.rest.RestClient;
import com.example.androidtestingisfun.chargeback.rest.RestService;
import com.example.androidtestingisfun.chargeback.ui.ChargebackActivity;
import com.example.androidtestingisfun.util.RestServiceMock;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.MockitoAnnotations.Mock;

import retrofit2.mock.BehaviorDelegate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;


public class ChargebackPresenterTest {

    ChargebackPresenter presenter;
    RestServiceMock service;

    @Mock
    ChargebackActivity view;

    ChargebackPresenter spy;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        // mock the rest api
        BehaviorDelegate<RestService> delegate = RestClient.getMockInstance().create(RestService.class);
        service = new RestServiceMock(delegate);

        // instanceate SUT
        presenter = new ChargebackPresenter(view,
                RestServiceMock.getHappyChargebackResult(),
                service);

        spy = spy(presenter);
    }

    @Test
    public void onCreated_propertiesMatchPassedScreenContent(){
        // setup
        ChargebackContentResult passed = RestServiceMock.getHappyChargebackResult();
        // check
        assertEquals(presenter.titleLabel, passed.title );
        assertEquals(presenter.reason1.id, passed.reason_details.get(0).id );
        assertEquals(presenter.reason1.title, passed.reason_details.get(0).title );
        assertEquals(presenter.reason2.id, passed.reason_details.get(1).id );
        assertEquals(presenter.reason2.title, passed.reason_details.get(1).title );
        assertEquals(presenter.commentHint, passed.comment_hint);
    }

    @Test
    public void setReason1Selected_updatesMemberValue() {

        presenter.setReason1Selected(true);
        assertEquals(true, presenter.userSelectedReason1);
        presenter.setReason1Selected(false);
        assertEquals(false, presenter.userSelectedReason1);
    }

    @Test
    public void setReason2Selected_updatesMemberValue() {

        presenter.setReason2Selected(true);
        assertEquals(true, presenter.userSelectedReason2);
        presenter.setReason2Selected(false);
        assertEquals(false, presenter.userSelectedReason2);
    }

    @Test
    public void onPrimaryAction_callsSubmitChargeback() {
        // setup
        doNothing().when(spy).submitChargeBack();
        // exercise
        spy.onPrimaryUserAction();
        // check
        verify(spy).submitChargeBack();
    }

    @Test
    public void onSecondaryAction_ViewFinishes() throws Exception {
        // exercise
        spy.onSecondaryUserAction();
        // check
        verify(spy.view).finish();
    }






}