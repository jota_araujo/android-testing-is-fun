package com.example.androidtestingisfun;

import com.example.androidtestingisfun.chargeback.models.NoticeContentResult;
import com.example.androidtestingisfun.chargeback.presenter.NoticePresenter;
import com.example.androidtestingisfun.chargeback.rest.RestClient;
import com.example.androidtestingisfun.chargeback.rest.RestService;
import com.example.androidtestingisfun.chargeback.ui.NoticeActivity;
import com.example.androidtestingisfun.util.RestServiceMock;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.MockitoAnnotations.Mock;

import retrofit2.mock.BehaviorDelegate;
import rx.schedulers.Schedulers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;


public class NoticePresenterTest {

    NoticePresenter presenter;
    RestServiceMock service;

    @Mock
    NoticeActivity view;

    NoticePresenter spy;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        // mock the rest api
        BehaviorDelegate<RestService> delegate = RestClient.getMockInstance().create(RestService.class);
        service = new RestServiceMock(delegate);

        // instanceate SUT
        presenter = new NoticePresenter(view,
                                        RestServiceMock.getHappyNoticeResult(),
                                        service);

        spy = spy(presenter);
    }

    @Test
    public void onCreated_propertiesMatchPassedScreenContent(){
        // setup
        NoticeContentResult passed = RestServiceMock.getHappyNoticeResult();
        // check
        assertEquals(presenter.titleLabel,passed.title );
        assertEquals(presenter.descriptionLabel ,passed.description );
        assertEquals(presenter.primaryButtonLabel ,passed.primary_action.title );
        assertEquals(presenter.secondaryButtonLabel ,passed.secondary_action.title );
        assertEquals(presenter.chargebackLink.href, passed.links.chargeback.href);
    }

    @Test
    public void onPrimaryActionCalled_callsLoadChargebackFunction() {
        // setup
        doNothing().when(spy).loadChargebackScreenContent();
        // exercise
        spy.onPrimaryUserAction();
        // check
        verify(spy).loadChargebackScreenContent();
    }

    @Test
    public void onSecondaryAction_ViewFinishes() throws Exception {
        // exercise
        spy.onSecondaryUserAction();
        // check
        verify(spy.view).finish();
    }

    @Test
    public void onPrimaryAction_CallsNavigateToChargeback() throws Exception {

        // setup
        spy.schedulerForSubscribe = Schedulers.immediate();
        spy.schedulerForObserve = Schedulers.immediate();
        doNothing().when(spy).loadChargebackScreenContent();

        // exercise
        spy.onPrimaryUserAction();

        // check
        verify(spy).loadChargebackScreenContent();
    }








}